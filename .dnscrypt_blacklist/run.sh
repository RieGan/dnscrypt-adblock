#!/bin/bash


echo "=== DNSCrypt adblocking software ==="
echo " 1. update adblock list "
echo " 2. reset adblock config "

read varInput

echo "----------------------------------"
cd ~/.dnscrypt_blacklist

if [[ $varInput == 1 ]]
then
echo "Updating DNS-Crypt ad Blacklist"
echo "===> downloading list from internet"

python3 generate-domains-blacklist.py > list.txt.tmp && mv -f list.txt.tmp list

echo "===> move the list to dnscrypt dir"

sudo mv list /etc/dnscrypt-proxy/blacklist.txt

echo "===> restarting dnscrypt"

sudo systemctl restart dnscrypt-proxy

echo "Finished, flush your DNS cache or restart"
fi

if [[ $varInput == 2 ]]
then
echo "Reseting adblock config"

echo "===> removing old config & script"
rm generate-domains-blacklist.py
rm domains-blacklist.conf
rm domains-blacklist-local-additions.txt
rm domains-time-restricted.txt
rm domains-whitelist.txt

echo "===> downloading config & script from internet"
wget https://raw.githubusercontent.com/DNSCrypt/dnscrypt-proxy/master/utils/generate-domains-blacklists/generate-domains-blacklist.py
wget https://raw.githubusercontent.com/DNSCrypt/dnscrypt-proxy/master/utils/generate-domains-blacklists/generate-domains-blacklist.py
wget https://raw.githubusercontent.com/DNSCrypt/dnscrypt-proxy/master/utils/generate-domains-blacklists/domains-blacklist.conf
wget https://raw.githubusercontent.com/DNSCrypt/dnscrypt-proxy/master/utils/generate-domains-blacklists/domains-blacklist-local-additions.txt
wget https://raw.githubusercontent.com/DNSCrypt/dnscrypt-proxy/master/utils/generate-domains-blacklists/domains-time-restricted.txt
wget https://raw.githubusercontent.com/DNSCrypt/dnscrypt-proxy/master/utils/generate-domains-blacklists/domains-whitelist.txt

echo "Updating DNS-Crypt ad Blacklist"
echo "===> downloading list from internet"

python3 generate-domains-blacklist.py > list.txt.tmp && mv -f list.txt.tmp list

echo "===> move the list to dnscrypt dir"

sudo mv list /etc/dnscrypt-proxy/blacklist.txt

echo "===> restarting dnscrypt"

sudo systemctl restart dnscrypt-proxy

echo "Finished, flush your DNS cache or restart"
fi

